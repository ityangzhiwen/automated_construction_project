#### 项目结构
config
    *.dev.js
    *.prod.js
    index.js
dist
dev
sources
src
    sass
    img
    js
    pages
        *.html
gulpfile.js
package.json
......


fileinclude = require('gulp-file-include') // html引用
下面的传参一定要用双引号
```html
@@include("src/footer.html", {
    "footer1": "home-footer1",
    "footer2": "home-footer2",
    "footer": {
        "footer1": "home-footer-footer1",
        "footer2": "home-footer-footer2"
    }
})
```

对插件源码做一些小的修改, 就可以将a-5da2910089.css模式转变成更加方便的a.css?v=5da2910089模式
```js
第一步: 进入/node_modules/gulp-rev/index.js修改代码: 第135行
将return through.obj((file, enc, cb) => {
        // Ignore all non-rev'd files
        if (!file.path || !file.revOrigPath) {
            cb();
            return;
        }

        const revisionedFile = relPath(path.resolve(file.cwd, file.base), path.resolve(file.cwd, file.path));
        const originalFile = path.join(path.dirname(revisionedFile), path.basename(file.revOrigPath)).replace(/\\/g, '/');

        manifest[originalFile] = revisionedFile;//这段删除
        manifest[originalFile] = originalFile + '?v=' + file.revHash;//这段添加
        cb();
}


第二步: 进入/node_modules/rev-path/index.js修改代码:
将第一个函数中的:return modifyFilename(pth, (filename, ext) => `${filename}-${hash}${ext}`); 里边的-${hash}删掉;
将第一个函数中的:return modifyFilename(pth, (filename, ext) => filename.replace(new RegExp(`-${hash}$`), '')  + ext); 里边的.replace(new RegExp(`-${hash}$`), '') 删掉;

第三步: 进入/node_modules/gulp-rev-collector/index.js修改代码:(更改两处)
1) 140 和 160行
      // regexp: new RegExp(  dirRule.dirRX + pattern, 'g' ),//删除这段
      regexp: new RegExp( dirRule.dirRX  + pattern+'(\\?v=\\w{10})?', 'g' ),//添加这段
 // regexp: new RegExp( prefixDelim + pattern, 'g' ),//删除这段
  regexp: new RegExp( prefixDelim + pattern + '(\\?v=\\w{10})?', 'g' ),//添加这段

2)  40行
   // var cleanReplacement =  path.basename(json[key]).replace(new RegExp(   opts.revSuffix ), '' );//删除
    var cleanReplacement = path.basename(json[key]).split('?')[0];//添加
```

"babel-preset-env": "^1.7.0",
"babel-preset-es2015": "^6.24.1",
