var gulp = require('gulp'), // 基础库
  plumber = require('gulp-plumber'), // 检测错误
  htmlmin = require('gulp-htmlmin'), // html压缩
  sass = require('gulp-sass'),
  uglify = require('gulp-uglify'), // js压缩
  babel = require('gulp-babel'),
  gutil = require('gulp-util'), // 如果有自定义方法，会用到
  imagemin = require('gulp-imagemin'), // 图片压缩
  cache = require('gulp-cache'), // 图片压缩缓存
  fileinclude = require('gulp-file-include'), //html引用
  rev = require('gulp-rev'), // 添加版本号
  revCollector = require('gulp-rev-collector'), // gulp-rev的插件，改变html的路径
  autoprefixer = require('gulp-autoprefixer'),
  runSequence = require('run-sequence'), //同步
  clean = require('gulp-clean'); // 清空文件夹
// const $ = require('gulp-load-plugins')();

const config = require('./index.js'); //引用配置的路径文件

function prod() {
  function errrHandler(e) {
    // 控制台发声,错误时beep一下
    gutil.beep();
    gutil.log(e);
    this.emit('end');
  };
  gulp.task('clean', function () {
    return gulp.src(config.dist, {
        read: false
      })
      .pipe(clean());
  });

  gulp.task('html:prod', function () {
    var options = {
      removeComments: true, // 清除HTML注释
      collapseWhitespace: true, // 压缩HTML
      collapseBooleanAttributes: true, // 省略布尔属性的值 <input checked="true"/> ==> <input />
      removeEmptyAttributes: true, // 删除所有空格作属性值 <input id="" /> ==> <input />
      removeScriptTypeAttributes: true, // 删除<script>的type="text/javascript"
      removeStyleLinkTypeAttributes: true, // 删除<style>和<link>的type="text/css"
      minifyJS: true, // 压缩页面JS
      minifyCSS: true // 压缩页面CSS
    };
    return gulp.src(['sources/rev/**/*.json', config.html.src])
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      // .pipe(processhtml())
      .pipe(fileinclude({
        prefix: '@@',
        basepath: config.src,
        indent: true
      }))
      // 原来的Html引入静态资源的时候，路径就要预先写好，然后rev来帮你修改后缀。
      .pipe(revCollector({
        replaceReved: true,
        dirReplacements: {
          'css': 'css',
          'js': 'js'
        }
      }))
      .pipe(htmlmin(options))
      .pipe(gulp.dest(config.html.dist))
  });
  // gulp-rev插件只能添加后缀, 不能讲html里的路径替换, 如果想要替换路径, 就需要gulp-rev-collector
  gulp.task('css:prod', function () {
    return gulp.src(config.sass.src)
      .pipe(sass({
        outputStyle: 'compressed'
      }))
      .pipe(autoprefixer({
        browseers: ['last 2 versions', 'Firefox>=20', 'last 2 Explorer versions', 'last 3 Safari versions'],
        cascade: true
      }))
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      .pipe(rev()) // 添加hash后缀
      .pipe(gulp.dest(config.sass.dist))
      .pipe(rev.manifest())
      .pipe(gulp.dest(config.sources.css))
  });

  gulp.task('js:prod', function () {
    return gulp.src(config.js.src)
      .pipe(babel({
        presets: ['env', 'es2015']
      }))
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      .pipe(uglify({
        mangle: {
          except: ['require', 'exports', 'module', '$']
        }, // 类型：Boolean 默认：true 是否修改变量名
        compress: true, // 类型：Boolean 默认：true 是否完全压缩
        preserveComments: 'false' // 保留所有注释
      }))
      .pipe(rev()) // 添加hash后缀
      .pipe(gulp.dest(config.js.dist))
      .pipe(rev.manifest())
      .pipe(gulp.dest(config.sources.js))
  });

  gulp.task('img:prod', function () {
    return gulp.src(config.img.src)
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      .pipe(cache(imagemin()))
      .pipe(gulp.dest(config.img.dist))
  });

  gulp.task('static:prod', function () {
    return gulp.src(config.static.src)
      .pipe(gulp.dest(config.static.dist))
  });

  // 打包构建所有的文件
  gulp.task('prod', function (cb) {
    runSequence(
      'clean',
      'css:prod', 'js:prod', 'img:prod', 'static:prod', 'html:prod'
    )
  });
};

module.exports = prod;