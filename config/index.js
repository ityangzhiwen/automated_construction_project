const SRC_DIR = './src/'; //源路径，不支持相对路径，下面也是
const DEV_DIR = './dev/'; //生成开发环境的文件
const DIST_DIR = './dist/'; //生成生产环境的文件
const SOURCES_DIR = './sources/'; //生成生产环境的文件


const config = {
  src: SRC_DIR,
  dev: DEV_DIR,
  dist: DIST_DIR,
  html: {
    src: SRC_DIR + 'pages/**/*.html',
    dev: DEV_DIR,
    dist: DIST_DIR
  },
  static: {
    src: SRC_DIR + 'static/**/*',
    dev: DEV_DIR + 'static',
    dist: DIST_DIR + 'static'
  },
  sass: {
    src: SRC_DIR + 'sass/**/*.scss', //如果是scss或者css，就改对应的
    dev: DEV_DIR + 'css',
    dist: DIST_DIR + 'css'
  },
  js: {
    src: SRC_DIR + 'js/**/*.js',
    dev: DEV_DIR + 'js',
    dist: DIST_DIR + 'js'
  },
  img: {
    src: SRC_DIR + 'img/**/*.{png,jpg,jpeg,gif}',
    dev: DEV_DIR + 'img',
    dist: DIST_DIR + 'img'
  },
  sources: {
    src: SOURCES_DIR,
    css: SOURCES_DIR + 'rev/css',
    js: SOURCES_DIR + 'rev/js'
  }
};

module.exports = config; //把这个接口暴露给别的文件用