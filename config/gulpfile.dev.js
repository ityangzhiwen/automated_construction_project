var gulp = require('gulp'), // 基础库
  plumber = require('gulp-plumber'), // 检测错误
  clean = require('gulp-clean'), // 清空文件夹
  sass = require('gulp-sass'),
  babel = require('gulp-babel'),
  gutil = require('gulp-util'), // 如果有自定义方法，会用到
  imagemin = require('gulp-imagemin'), // 图片压缩
  cache = require('gulp-cache'), // 图片压缩缓存
  connect = require('gulp-connect'), // 生成本地服务器及实现热加载功能
  fileinclude = require('gulp-file-include'), //html引用
  autoprefixer = require('gulp-autoprefixer'),
  open = require('open'), //开启服务
  runSequence = require('run-sequence'); //同步

const config = require('./index.js'); //引用配置的路径文件


function dev() {
  function errrHandler(e) {
    // 控制台发声,错误时beep一下
    gutil.beep();
    gutil.log(e);
    this.emit('end');
  };
  gulp.task('clean', function () {
    return gulp.src(config.dev, {
        read: false
      })
      .pipe(clean());
  });

  gulp.task('html:dev', function () {
    return gulp.src(config.html.src)
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      .pipe(fileinclude({
        prefix: '@@',
        basepath: config.src,
        indent: true
      }))
      .pipe(gulp.dest(config.html.dev))
      .pipe(connect.reload())
  });


  gulp.task('css:dev', function () {
    return gulp.src(config.sass.src)
      .pipe(sass())
      .pipe(autoprefixer({
        browseers: ['last 2 versions', 'Firefox>=20', 'last 2 Explorer versions', 'last 3 Safari versions'],
        cascade: true
      }))
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      .pipe(gulp.dest(config.sass.dev))
      .pipe(connect.reload())
  });

  gulp.task('js:dev', function () {
    return gulp.src(config.js.src)
      .pipe(babel({
        presets: ['env']
      }))
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      .pipe(gulp.dest(config.js.dev))
      .pipe(connect.reload())
  });

  gulp.task('img:dev', function () {
    return gulp.src(config.img.src)
      .pipe(plumber({
        errorHandler: errrHandler
      }))
      .pipe(cache(imagemin()))
      .pipe(gulp.dest(config.img.dev))
      .pipe(connect.reload())
  });

  gulp.task('static:dev', function () {
    return gulp.src(config.static.src)
      .pipe(gulp.dest(config.static.dev))
      .pipe(connect.reload())
  });


  // 自动创建文件并监听修改
  gulp.task('auto', function () {
    // 监听当前目录下的文件夹，源码有改动就进行压缩以及热刷新
    gulp.watch(['src/**/*.html'], ['html:dev'])
    gulp.watch(config.sass.src, ['css:dev'])
    gulp.watch(config.js.src, ['js:dev'])
    gulp.watch(config.img.src, ['img:dev'])
    gulp.watch(config.static.src, ['static:dev'])
  });

  // gulp服务器
  gulp.task('server', function (done) {
    connect.server({
      root: config.dev, //监听当前的文件夹
      port: 8888,
      livereload: true
    })
    // open('http://localhost:8888')
  });

  // 默认任务,自动编译文件
  gulp.task('dev', function (cb) {
    runSequence(
      'clean',
      'html:dev', 'css:dev', 'js:dev', 'img:dev', 'static:dev',
      ['server', 'auto']
    )
  });
};

module.exports = dev;